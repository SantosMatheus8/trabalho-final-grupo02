## Ata de reuniões - Trabalho Final Grupo 02

### Reunião inicial - 17/07/2022

Membros reunidos: Matheus, Ana, João, Gabriel e Felipe.

- Reconhecimento de ambiente: grade horária de cada membro do grupo, disponibilidade e análise de pontos fortes e fracos de cada membro.

- Criamos critérios para eleger o porta voz do grupo, levando em consideração tempo e disponibilidade, tarefa esta que foi incumbida ao Matheus.

- Realizamos uma breve análise do que seria crível ou não como meta semanal (17/07/2022 - 23/07/2022). Levando em conta que estas métricas são adaptáveis, supomos que era possível:

- Identidicar todas as entidades
- Diagrama entidade relacionamento
- Diagrama lógico
- Identificar todas as rotas
- Organizar nosso workflow (gitflow)
- Criar migrations
- Criar entities

### Reunião - 19/07/2022

Membros reunidos: Matheus, Ana e João.

- Fizemos o push da nossa base do projeto no gitlab.

- Organizamos o modelo de trabalho e como vão ser feitas as issues.

- Checamos e revisamos as entidades/atributos na modelagem de dados e no diagrama lógico.

- Fizemos uma espécia de "fichamento" do enunciado do trabalho para eventuais questionamentos em relação à modelagem de dados, tabela de entidades e também ao prosseguimento geral do nosso trabalho.

- Limpamos a modelagem e adicionamos as cardinalidades.

### Reunião - 20/07/2022

Membros reunidos: Matheus, Ana, João e Felipe.

- Finalizamos o diagrama lógico e a modelagem de dados.

- Criamos a ata de reuniões do projeto.

- Estudamos sobre gitflow para melhorarmos nosso fluxo de trabalho.

- Fizemos o mapeamento das rotas.

### Reunião - 24/07/2022

Membros reunidos: Matheus, Ana, João e Felipe.

- Redistribuimos e finalizamos as issues que faltavam da semana 1.

- Debatemos sobre os padrões das rotas e dos códigos no geral.

- Separamos duplas para trabalhar nas rotas, para que haja um melhor workflow.

- Configuramos o swagger dentro do projeto.

- Definimos nossa divisão de tarefas para as semanas 2 e 3, que está, também, devidamente organizada na área de milestones do gitlab.

### Reunião - 28/07/2022

Membros reunidos: Matheus, Ana, João e Felipe.

- Reanalisamos as rotas e começamos a testar as rotas.

- Alterações em algumas rotas em virtude dos testes.

- Atualização do swagger.

- Estamos pavimentando o início da autenticação, faltando apenas a correção de alguns detalhes e terminar os testes.

### Reunião - 29/07/2022

Membros reunidos: Matheus, Ana, João e Felipe.

- Começamos de fato a implementação da autenticação de usuário, programando em grupo utilizando a extensão "Live Share".

### Reunião - 31/07/2022

Membros reunidos: Matheus, Ana, João e Felipe.

- Finalizamos a implementação da autenticação de usuário, programando em grupo utilizando a extensão "Live Share".

### Reunião - 01/08/2022

Membros reunidos: Matheus, Ana e Felipe.

- Fizemos a parte de upload de arquivos, utilizando a extensão "Live Share" e planejamos as atividades do restante da semana.

### Reunião - 02/08/2022

Membros reunidos: Matheus, Ana e Felipe.

- Fizemos a parte de download de arquivos utilizando a extensão "Live Share".

### Reunião - 03/08/2022

Membros reunidos: Matheus, Ana e Felipe.

- Nos reunimos para nos ajudar em alguns erros que estavamos enfrentando.

### Reunião - 04/08/2022

Membros reunidos: Matheus, Ana e Felipe.

- Fizemos usando a extensão "Live Shave", a rota que envia email para que um novo aluno possa ser cadastrado.

### Reunião - 05/08/2022

Membros reunidos: Matheus, Ana e Felipe.

- Fizemos usando a extensão "Live Shave", a rota que envia email e permite que a senha do usuário seja redefinida.
